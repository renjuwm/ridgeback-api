var Mongoose = require('mongoose');
var constants = require('./../config/constants');

Mongoose.connect('mongodb://' + constants.database.host + ':' + constants.database.port +'/ridgeback');

var db = Mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', function callback() {
    console.log("Connection with database succeeded. HOST: "+ constants.database.host +':' + constants.database.port);
});
exports.Mongoose = Mongoose;
exports.db = db;