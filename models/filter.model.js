/****
 * Created by Renju
 */
'use strict';


//TODO: skip and take should be removed from Collection. take it as parameter from request
let mongoose = require('mongoose');

let filterSchema = new mongoose.Schema({
        name: {
            type: String,
            required: true,
            trim: true
        },
        description: {
            type: String
        },
        all_condition: [{
            key: {
                type: String
            },
            condition: {
                type: String
            },
            value: {
                type: String
            }
        }],
        any_condition: [{
            key: {
                type: String
            },
            condition: {
                type: String
            },
            value: {
                type: String
            },
            type: {
                type: String
            }
        }],
        sort_by: {
            type: String
        },
        sort_order: {
            type: String
        },
        take: {
            type: Number
        },
        skip: {
            type: Number
        },
        status: {
            type: Number,
            required: true
        }
    },
    {
        timestamps: {
            createdAt: 'created_at',
            updatedAt: 'updated_at'
        }
    });


module.exports = {
    schema: filterSchema,
    model: mongoose.model('Filter', filterSchema)
};