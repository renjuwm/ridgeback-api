//server.js

var Hapi = require('hapi');
var constants = require('./config/constants');
var db = require('./lib/database');
var host = constants.application.host;
var port = constants.application.port;

var server = new Hapi.Server({});
server.connection({host: host, port: port, routes: { cors: true }});

server.ext({
    type: 'onRequest',
    method: function function_name(request, reply) {
        console.log("Request ", request.path, request.info);
        return reply.continue();
    }
});

server.register([{
    register: require('./plugins/issue/issue.plugin'),
    options: {
        message: 'Registered'
    },
    routes: {
        prefix: '/api/v1'
    }
}, {
    register: require('./plugins/filter/filter.plugin'),
    options: {
        message: 'Registered'
    },
    routes: {
        prefix: '/api/v1'
    }
}], (err) => {
    if (err) {
        console.log("Failed loading plugin");
    }
});

server.start(function () {
    console.log("Server Started " + server.info.uri);
});
