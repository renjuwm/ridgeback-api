//Filter Plugin

// Load Modules
var Filter = require('./filter.controller');

var filterPlugin = {
    register: function(server, options, next) {

        console.log("filter-plugin "+options.message );

        //Filter API Endpoints
        var endpoints = [
            { method: 'GET', path: '/filter', config: Filter.getFilters },
            { method: 'GET', path: '/filter/{filterId}', config: Filter.getById },
            { method: 'GET', path: '/filter/module/{module}', config: Filter.getModuleFilters },
            { method: 'GET', path: '/filter/module/{module}/name/{name}', config: Filter.getFiltersByName }
            ////{ method: 'POST', path: '/filter', config: Issue.createIssue},
        ];

        server.expose('filter_ctrl',Filter);

        //Set endpoints to Server Route
        server.route(endpoints);

        next();
    }
};

filterPlugin.register.attributes = {
    pkg: require('./package.json')
};

module.exports = filterPlugin;