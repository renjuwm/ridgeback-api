// filter.controllers.js

var Joi = require('joi');
var mongoose = require('mongoose');
var FilterModel = require('./../../models/filter.model').model;
var Boom = require('boom');


// Find First 10 News Items
//News.find({
//        deal_id:deal._id // Search Filters
//    },
//    ['type','date_added'], // Columns to Return
//    {
//        skip:0, // Starting Row
//        limit:10, // Ending Row
//        sort:{
//            date_added: -1 //Sort by Date Added DESC
//        }
//    },
//    function(err,allNews){
//        socket.emit('news-load', allNews); // Do something with the array of 10 objects
//    })
exports.getFilters = {
    handler: function (request, reply) {
        FilterModel.find({}, [], { sort: { filter_order: 1 }})
            .then((filters) => {
                return reply(filters || []);
            })
            .catch((exp) => {
                console.log("Filters Find Exception");
                reply(Boom.badImplementation("Filters Find Exception", exp));
            });
    }
};

exports.getById = {
    handler: function (request, reply) {

        var filterId = request.params.filterId;

        //noinspection JSUnresolvedVariable
        if (!mongoose.Types.ObjectId.isValid(filterId)) {
            return reply(Boom.badRequest("Invalid Request. Filter ID is not valid"));
        }

        FilterModel.findById(filterId)
            .then((filter) => {
                return reply(filter);
            })
            .catch((exp) => {
                console.log("Filters Find Exception");
                reply(Boom.badImplementation("Filters Find Exception", exp));
            });
    }
};


exports.getModuleFilters = {
    handler: function (request, reply) {

        var module = request.params.module;

        FilterModel.find({"module": module}, {}, { sort: { filter_order: 1 }})
            .then((filters) => {
                return reply(filters);
            })
            .catch((exp) => {
                console.log("Filters Find Exception");
                reply(Boom.badImplementation("Filters Find Exception", exp));
            });
    }
};


exports.getFiltersByName = {
    handler: function (request, reply) {

        var module = request.params.module;
        var name = request.params.name;

        FilterModel.find({ "module": module , "name": name })
            .then((filter) => {
                return reply(filter);
            })
            .catch((exp) => {
                console.log("Filters Find Exception");
                reply(Boom.badImplementation("Filters Find Exception", exp));
            });
    }
};
