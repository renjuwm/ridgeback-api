// issue.controllers.js

var Joi = require('joi');
//noinspection JSUnresolvedFunction
var IssueModule = require('issue');
var Issue = IssueModule.issue;
var IssueModel = IssueModule.issueModel;
var mongoose = require('mongoose');
var FilterModel = require('./../../models/filter.model').model;
var Boom = require('boom');

//noinspection JSUnusedGlobalSymbols
exports.getIssues = {

    handler: function (request, reply) {
        var filters = {
            all_condition: [
                { key: "status.id", condition: "$lte", value: "5" }
            ]
        };

        try {
            Issue.filter(filters, (error, result) => {
                console.log("Issue Filter Callback");
                if (error) {
                    return reply({
                        error: error
                    });
                } else {
                    return reply(result);
                }
            });
        }
        catch (exp) {
            console.log("Issue Filter Exception");
            return reply({
                Exception: exp
            });
        }
    }

};

exports.getById = {

    handler: function (request, reply) {
        var issueId = request.params.issueId;
        Issue.getByIssueId(issueId, (error, issue) => {
            console.log("Issue Callback");
            reply({
                issue: issue
            });
        });
    }
};

exports.createIssue = {

    handler: function (request, reply) {

        var newIssue = new IssueModel({
            subject: request.payload.subject,
            body: request.payload.body,
            priority: request.payload.priority,
            status: {
                id: request.payload.status.id,
                value: request.payload.status.value
            },
            channel: {
                channel_type: request.payload.channel.channel_type
            },
            customer: {
                objRef: mongoose.Types.ObjectId(),
                customer_display_name: request.payload.customer.customer_display_name
            }
        });

        try {
            Issue.create(newIssue, (error, issue) => {
                if (error) {
                    reply({
                        error: error
                    });
                } else {
                    reply({
                        issue: issue
                    });
                }

            });
        } catch (exp) {
            console.log("Issue Create Exception");
            reply({
                Exception: exp
            });
        }
    }
};


exports.filter = {

    handler: function (request, reply) {
        var filterId = request.params.filterId;

        if (!mongoose.Types.ObjectId.isValid(filterId)) {
            return reply({
                error: "Filter ID is not valid"
            });
        }

        FilterModel.findById(filterId)
            .then((filter) => {
                if (filter === null) {
                    return reply({
                        error: "Filter not found for Id " + filterId
                    });
                }
                try {
                    Issue.filter(filter.toJSON(), (error, result) => {
                        console.log("Issue Filter Callback");
                        if (error) {
                            return reply({
                                error: error
                            });
                        } else {
                            return reply(result);
                        }
                    });
                }
                catch (exp) {
                    console.log("Issue Filter Exception");
                    return reply({
                        Exception: exp
                    });
                }
            })
            .catch((exp) => {
                console.log("Filter Find Exception");
                return reply({
                    Exception: exp
                });
            });


    }
};



exports.filterByFilterName = {

    handler: function (request, reply) {

        var filterName = request.params.filterName;

        if (filterName === undefined || filterName === null) {
            return reply(Boom.badRequest("Invalid Request. Filter Name is not valid"));
        }

        FilterModel.findOne({ "module": "issue" , "name": filterName })
            .then((filter) => {
                if (filter === null) {
                    return reply(Boom.badRequest("Filter not found for name " + filterName +" in issue filter module"));
                }
                try {
                    //TODO : Should replace with logged in User ID
                    var replaceUserString = JSON.stringify(filter.toJSON()).replace(/{{current_user}}/g, "56f0b7b5fd55755c64631577");
                    var filterObj = JSON.parse(replaceUserString);

                    Issue.filter(filterObj, (error, result) => {
                        if (error) {
                            return reply(Boom.badImplementation("Issue Filter Exception", error));
                        } else {
                            return reply(result);
                        }
                    });
                }
                catch (exp) {
                    console.log("Issue Filter Exception");
                    return reply(Boom.badImplementation("Issue Filter Exception", exp));
                }
            })
            .catch((exp) => {
                console.log("Filter Find Exception");
                return reply(Boom.badImplementation("Filter Find Exception", exp));
            });
    }
};


exports.filterPost = {

    handler: function (request, reply) {
        console.log(" Filter Post Called");
        var filters = request.payload;

        if(filters === undefined){
            return reply(Boom.badRequest("Invalid Request. Required Filter Object"));
        }

        try {
            Issue.filter(filters, (error, result) => {
                console.log("Issue Filter Callback");
                if (error) {
                    return reply({
                        error: error
                    });
                } else {
                    return reply(result);
                }
            });
        }
        catch (exp) {
            console.log("Issue Filter Exception");
            return reply(Boom.badImplementation("Issue Filter Exception", exp));
        }
    }
};
