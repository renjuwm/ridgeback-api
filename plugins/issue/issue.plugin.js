//Issue Plugin

// Load Modules
var Issue = require('./issue.controller');

var issuePlugin = {
    register: function(server, options, next) {

        console.log("Registerd "+options.message );
        //Issue API Endpoints 
        var endpoints = [
            { method: 'GET', path: '/issue', config: Issue.getIssues },
            { method: 'GET', path: '/issue/{issueId}', config: Issue.getById },
            { method: 'POST', path: '/issue', config: Issue.createIssue},
            { method: 'GET', path: '/issue/filter/{filterId}', config: Issue.filter},
            { method: 'POST', path: '/issue/filter', config: Issue.filterPost},
            { method: 'GET', path: '/issue/filter/name/{filterName}', config: Issue.filterByFilterName}
        ];

        //Set endpoints to Server Route
        server.route(endpoints);

        next();
    }
};

issuePlugin.register.attributes = {
    pkg: require('./package.json')
};

module.exports = issuePlugin;